$('.dropify').dropify({
    messages: {
        'default': 'قم بسحب وإفلات ملف أو إضغط هنا',
        'replace': 'قم بسحب وإفلات ملف هنا أو إضغط لتغيير الصورة',
        'remove': 'إزالة',
        'error': 'حدث خطأ ما'
    },
    error: {
        'fileSize': 'The file size is too big (1M max).'
    }
});