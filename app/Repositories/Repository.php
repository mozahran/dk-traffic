<?php

namespace App\Repositories;

use App\Repositories\Exceptions\RepositoryException;
use App\Repositories\Interfaces\RepositoryInterface;
use App\Repositories\Traits\Relationable;
use Illuminate\Database\Eloquent\Model;

abstract class Repository implements RepositoryInterface
{
    use Relationable;

    /**
     * A reference to the eloquent model;
     *
     * @var \Illuminate\Database\Eloquent\Model;
     */
    public $model;

    /**
     * Repository constructor.
     */
    public function __construct()
    {
        $this->getInstance();
    }

    /**
     * Sets the name of the eloquent model.
     *
     * @return mixed
     */
    public abstract function model();

    /**
     * Get instance of the eloquent model that the repository belongs to.
     * @return Model|mixed
     * @throws RepositoryException
     */
    public function getInstance()
    {
        $model = app()->make($this->model());

        if ( ! $model instanceof Model) {
            throw new RepositoryException("Class {$this->model()} must be an instance of Illuminate\\Database\\Eloquent\\Model");
        }

        return $this->model = $model;
    }

    /**
     * Get all the models.
     *
     * @param string|array $columns
     * @return mixed
     */
    public function all($columns = ['*'])
    {
        return $this->getInstance()
                    ->with($this->relations)
                    ->latest()
                    ->get($columns);
    }

    /**
     * Paginate the models.
     *
     * @param int $perPage
     * @param string|array $columns
     * @return mixed
     */
    public function paginate($perPage = 25, $columns = ['*'])
    {
        return $this->getInstance()
                    ->with($this->relations)
                    ->latest()
                    ->paginate($perPage, $columns);
    }

    /**
     * Create a new model.
     *
     * @param array $attributes
     * @return mixed
     */
    public function create(array $attributes)
    {
        return $this->getInstance()
                    ->create($attributes);
    }

    /**
     * Make a new model.
     *
     * @param array $attributes
     * @return mixed
     */
    public function make(array $attributes = [])
    {
        return (new $this->model)->fill($attributes);
    }

    /**
     * Update an existing model.
     *
     * @param array $attributes
     * @param int $id
     * @param string $attribute
     * @return mixed
     */
    public function update(array $attributes, $id, $attribute = 'id')
    {
        return $this->getInstance()
                    ->where($attribute, $id)
                    ->update($attributes);
    }

    /**
     * Delete an existing model or destroy a collection of existing models.
     *
     * @param int|array $id
     * @param string $attribute
     * @return mixed
     */
    public function delete($id, $attribute = null)
    {
        if ($attribute != null) {
            return $this->getInstance()
                        ->where($attribute, $id)
                        ->delete();
        }

        return $this->getInstance()->destroy($id);
    }

    /**
     * Find a model by Id.
     *
     * @param int $id
     * @param array $columns
     * @return mixed
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function find($id, $columns = ['*'])
    {
        return $this->getInstance()
                    ->with($this->relations)
                    ->findOrFail((int) $id, $columns);
    }

    /**
     * Find a model by attribute.
     *
     * @param string $attribute
     * @param mixed $value
     * @param array $columns
     * @return mixed
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function findBy($attribute, $value, $columns = ['*'])
    {
        return $this->getInstance()
                    ->with($this->relations)
                    ->where($attribute, $value)
                    ->firstOrFail($columns);
    }

    /**
     * Search for models.
     *
     * @param string $q
     * @param string $column
     * @return mixed
     */
    public function search($q, $column)
    {
        return $this->getInstance()
                    ->with($this->relations)
                    ->where($column, 'like', '%' . $q . '%')
                    ->get();
    }

    /**
     * Count all methods.
     *
     * @return int
     */
    public function countAll()
    {
        return $this->getInstance()->count();
    }
}