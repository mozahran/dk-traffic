<?php

namespace App\Repositories;

use App\Car;

class CarRepository extends Repository
{
    /**
     * Sets the name of the eloquent model.
     *
     * @return mixed
     */
    public function model()
    {
        return Car::class;
    }
}