<?php

namespace App\Repositories\Traits;

trait Relationable
{
    public $relations = [];
    public $relationsCount = [];

    /**
     * Set the relations that are returned with the model.
     *
     * @param array|null $relations
     */
    public function setRelations(array $relations = null)
    {
        $this->relations = $relations;
    }

    /**
     * Set the relations that are going to be counted and returned with the model.
     *
     * @param array|null $relations
     */
    public function setRelationsCount(array $relations = null)
    {
        $this->relationsCount = $relations;
    }
}