<?php

namespace App\Repositories;

use App\Code;

class CodeRepository extends Repository
{
    /**
     * Sets the name of the eloquent model.
     *
     * @return mixed
     */
    public function model()
    {
        return Code::class;
    }

    /**
     * Paginate the models.
     *
     * @param int $perPage
     * @param string|array $columns
     * @return mixed
     */
    public function paginate($perPage = 25, $columns = ['*'])
    {
        return $this->getInstance()
                    ->with($this->relations)
                    ->orderBy(Code::FIELD_SERIAL_NUMBER, 'desc')
                    ->paginate($perPage, $columns);
    }
}