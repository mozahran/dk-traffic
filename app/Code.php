<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Code extends Model
{
    /**
     * The table schema.
     */

    const TABLE_NAME = 'codes';

    const FIELD_PK = 'id';
    const FIELD_CAR_ID = 'car_id';
    const FIELD_SERIAL_NUMBER = 'serial_number';
    const FIELD_CODE = 'code';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        self::FIELD_CAR_ID,
        self::FIELD_SERIAL_NUMBER,
        self::FIELD_CODE,
    ];

    /*
	|--------------------------------------------------------------------------
	| Getters
	|--------------------------------------------------------------------------
    */

    /**
     * Get the user id.
     *
     * @return int
     */
    public function getId()
    {
        return (int) $this->getAttribute(static::FIELD_PK);
    }

    /**
     * Get the car id.
     *
     * @return int
     */
    public function getCarId()
    {
        return (int) $this->getAttribute(static::FIELD_CAR_ID);
    }

    /**
     * Get the serial number.
     *
     * @return int
     */
    public function getSerialNumber() : int
    {
        return $this->getAttribute(static::FIELD_SERIAL_NUMBER);
    }

    /**
     * Get the code.
     *
     * @return int
     */
    public function getCode()
    {
        return (int) $this->getAttribute(static::FIELD_CODE);
    }

    /*
	|--------------------------------------------------------------------------
	| Setters
	|--------------------------------------------------------------------------
    */

    /**
     * Set the car id.
     *
     * @param int $id
     */
    public function setCarId(int $id)
    {
        $this->setAttribute(static::FIELD_CAR_ID, $id);
    }

    /**
     * Set the serial number.
     *
     * @param int $n
     */
    public function setSerialNumber(int $n)
    {
        $this->setAttribute(static::FIELD_SERIAL_NUMBER, $n);
    }

    /**
     * Set the code.
     *
     * @param int $code
     */
    public function setCode(int $code)
    {
        $this->setAttribute(static::FIELD_CODE, $code);
    }

    /*
	|--------------------------------------------------------------------------
	| Relationships
	|--------------------------------------------------------------------------
    */

    /**
     * The car that the code belongs to
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function car()
    {
        return $this->belongsTo(Car::class)->withDefault([
            Car::FIELD_PK => null,
            Car::FIELD_MANUFACTURER => null,
            Car::FIELD_PLATE_NUMBER => null,
            Car::FIELD_MODEL => null,
            Car::FIELD_MODEL_YEAR => null,
            Car::FIELD_DRIVER_ID => null,
        ]);
    }
}
