<?php

if ( ! function_exists('public_asset'))
{
    function public_asset($path)
    {
        return asset($path);
        return asset('public/' . $path);
    }
}

if ( ! function_exists('set_title'))
{
    function set_title($title)
    {
        view()->share('pageTitle', $title);
    }
}

if ( ! function_exists('set_search_action'))
{
    function set_search_action($search_action)
    {
        view()->share('searchAction', $search_action);
    }
}

if ( ! function_exists('enable_edit_mode'))
{
    function enable_edit_mode()
    {
        view()->share('editMode', true);
    }
}

if ( ! function_exists('redirect_success'))
{
    function redirect_success($route, $status)
    {
        $toastr = 'success';

        return redirect($route)->with(compact('status', 'toastr'));
    }
}

if ( ! function_exists('redirect_fail'))
{
    function redirect_fail($route, $status)
    {
        $toastr = 'error';

        return redirect($route)->withInput()->with(compact('status', 'toastr'));
    }
}