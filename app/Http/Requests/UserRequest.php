<?php

namespace App\Http\Requests;

use App\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        $rules = [
            'name' => 'required',
        ];

        if ($this->method('put')) {
            $rules['email'] = sprintf('required|unique:%s,%s,%d', User::TABLE_NAME, User::FIELD_EMAIL, request('user_id'));
        } else {
            $rules['email'] = sprintf('required|unique:%s', User::TABLE_NAME);
        }

        return $rules;
    }
}
