<?php

namespace App\Services;

use DB;
use App\Car;
use App\Code;
use App\Driver;
use App\Repositories\CarRepository;
use App\Repositories\CodeRepository;
use App\Http\Requests\CodeRequest;
use App\Repositories\DriverRepository;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class CodeService
{
    /**
     * @var CodeRepository
     */
    private $codeRepository;

    /**
     * @var CarRepository
     */
    private $carRepository;

    /**
     * @var DriverRepository
     */
    private $driverRepository;

    /**
     * CodeService constructor.
     *
     * @param CodeRepository $codeRepository
     * @param CarRepository $carRepository
     * @param DriverRepository $driverRepository
     */
    public function __construct(
        CodeRepository $codeRepository,
        CarRepository $carRepository,
        DriverRepository $driverRepository
    )
    {
        $this->codeRepository = $codeRepository;
        $this->carRepository = $carRepository;
        $this->driverRepository = $driverRepository;
    }

    /**
     * Find a code by id.
     *
     * @param int $id
     * @return mixed
     */
    public function findById(int $id)
    {
        return $this->codeRepository->find($id);
    }

    /**
     * Create a new code.
     *
     * @param $driverName
     * @param $driverPhoneNumber
     * @param $carPlateNumber
     * @param $carManufacturer
     * @param $carModel
     * @param $carModelYear
     * @param $serialNumber
     * @param $carCode
     *
     * @return bool|\Exception
     */
    public function create(
        $driverName,
        $driverPhoneNumber,
        $carPlateNumber,
        $carManufacturer,
        $carModel,
        $carModelYear,
        $serialNumber,
        $carCode
    )
    {
        DB::beginTransaction();

        try {

            $driver = $this->driverRepository->create([
                Driver::FIELD_NAME => $driverName,
                Driver::FIELD_PHONE_NUMBER => $driverPhoneNumber,
            ]);

            $car = $this->carRepository->create([
                Car::FIELD_DRIVER_ID => $driver->getId(),
                Car::FIELD_PLATE_NUMBER => $carPlateNumber,
                Car::FIELD_MANUFACTURER => $carManufacturer,
                Car::FIELD_MODEL => $carModel,
                Car::FIELD_MODEL_YEAR => $carModelYear,
            ]);

            $this->codeRepository->create([
                Code::FIELD_CAR_ID => $car->getId(),
                Code::FIELD_SERIAL_NUMBER => $serialNumber,
                Code::FIELD_CODE => $carCode,
            ]);

            DB::commit();

            return true;

        } catch (\Exception $exception) {
            DB::rollback();
            return $exception;
        }
    }

    /**
     * Update an existing code.
     *
     * @param $id
     * @param $driverName
     * @param $driverPhoneNumber
     * @param $carPlateNumber
     * @param $carManufacturer
     * @param $carModel
     * @param $carModelYear
     * @param $serialNumber
     * @param $carCode
     *
     * @return bool|\Exception|\Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function update(
        $id,
        $driverName,
        $driverPhoneNumber,
        $carPlateNumber,
        $carManufacturer,
        $carModel,
        $carModelYear,
        $serialNumber,
        $carCode
    )
    {
        DB::beginTransaction();

        try {

            $this->codeRepository->setRelations([
                'car',
            ]);

            $code = $this->codeRepository->find($id);

            $carId = $code->car->getId();
            $driverId = $code->car->getDriverId();

            $this->driverRepository->update([
                Driver::FIELD_NAME => $driverName,
                Driver::FIELD_PHONE_NUMBER => $driverPhoneNumber,
            ], $driverId);

            $this->carRepository->update([
                Car::FIELD_DRIVER_ID => $driverId,
                Car::FIELD_PLATE_NUMBER => $carPlateNumber,
                Car::FIELD_MANUFACTURER => $carManufacturer,
                Car::FIELD_MODEL => $carModel,
                Car::FIELD_MODEL_YEAR => $carModelYear,
            ], $carId);


            $this->codeRepository->update([
                Code::FIELD_CAR_ID => $carId,
                Code::FIELD_SERIAL_NUMBER => $serialNumber,
                Code::FIELD_CODE => $carCode,
            ], $id);

            DB::commit();

            return true;

        } catch (ModelNotFoundException $exception) {
            DB::rollback();
            return $exception;
        } catch (\Exception $exception) {
            DB::rollback();
            return $exception;
        }
    }

    /**
     * Delete code and its relationships.
     *
     * @param $id
     * @return bool
     * @throws \Exception
     */
    public function delete($id)
    {
        DB::beginTransaction();

        try {

            $this->codeRepository->setRelations([
                'car.driver'
            ]);

            $code = $this->codeRepository->find($id);

            $this->driverRepository->delete($code->car->getDriverId());
            $this->carRepository->delete($code->car->getId());
            $this->codeRepository->delete($id);

            DB::commit();

            return true;

        } catch (\Exception $exception) {

            DB::rollback();

            throw new \Exception($exception->getMessage());
        }
    }

    /**
     * List codes & their relationships.
     *
     * @param int $perPage
     * @return mixed
     */
    public function all($perPage = 25)
    {
        $this->codeRepository->setRelations([
            'car.driver',
        ]);

        return $this->codeRepository->paginate((int)$perPage);
    }

    /**
     * Advanced search through codes and their relationships.
     *
     * @param $searchQuery
     * @param int $perPage
     * @param array $columns
     * @return mixed
     */
    function search($searchQuery, $perPage = 25, $columns = ['*'])
    {
        return $this
                ->codeRepository
                ->model
                ->orWhere(Code::FIELD_CODE, 'like', '%' . $searchQuery . '%')
                ->orWhereHas('car', function ($query) use ($searchQuery) {
                    return $query->where(Car::FIELD_PLATE_NUMBER, 'like', '%' . $searchQuery . '%');
                })->orWhereHas('car.driver', function ($query) use ($searchQuery) {
                    return $query->where(Driver::FIELD_NAME, 'like', '%' . $searchQuery . '%');
                })->paginate($perPage, $columns);
    }

    /**
     * Count all codes.
     *
     * @return int
     */
    public function countAll()
    {
        return $this->codeRepository->countAll();
    }

    /**
     * Reset serial numbers to match car codes.
     *
     * @return bool
     */
    public function resetSerialNumbers()
    {
        $codes = $this->codeRepository->all();

        foreach ($codes as $code)
        {
            $code->setSerialNumber($code->getCode());
            $code->save();
        }
    }
}