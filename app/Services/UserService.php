<?php

namespace App\Services;

use App\User;
use App\Http\Requests\UserRequest;
use App\Repositories\UserRepository;
use App\Services\Interfaces\ServiceInterface;
use App\Services\Interfaces\UserServiceInterface;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;

class UserService implements ServiceInterface
{
    /**
     * @var UserRepository
     */
    private $repository;

    /**
     * UserService constructor.
     *
     * @param UserRepository $repository
     */
    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Get an instance of the repository.
     *
     * @return UserRepository
     */
    public function getRepository()
    {
        return $this->repository;
    }

    /**
     * Find user by Id.
     *
     * @param int $id
     * @return User
     */
    public function findById(int $id): User
    {
        return $this->repository->find($id);
    }

    /**
     * Get all users.
     *
     * @param int $perPage
     * @param array $except
     * @return Collection
     */
    public function all(int $perPage = 25, array $except = []): LengthAwarePaginator
    {
        $protectedIds = array_unique($except);

        return $this->repository
                    ->getModel()
                    ->whereNotIn(User::FIELD_PK, $protectedIds)
                    ->paginate($perPage);
    }

    /**
     * Search for a user or a group of users.
     *
     * @param string $keyword
     * @param int $perPage
     * @return Collection
     */
    public function search(string $keyword, int $perPage = 25): LengthAwarePaginator
    {
        return $this->repository
                    ->getModel()
                    ->orWhere(User::FIELD_EMAIL, 'like', '%' . $keyword . '%')
                    ->orWhere(User::FIELD_NAME, 'like', '%' . $keyword . '%')
                    ->paginate($perPage);
    }

    /**
     * Create new user.
     *
     * @param $name
     * @param $email
     * @param $password
     *
     * @return \App\User
     */
    public function create($name, $email, $password): User
    {
        $attributes = [
            User::FIELD_NAME => $name,
            User::FIELD_EMAIL => $email,
        ];

        $attributes[User::FIELD_PASSWORD] = bcrypt($password);

        return $this->repository->create($attributes);
    }

    /**
     * Update a specific user.
     *
     * @param int  $id
     * @param      $name
     * @param      $email
     * @param bool $password
     *
     * @return bool
     */
    public function update(int $id, $name, $email, $password = false): bool
    {
        $attributes = [
            User::FIELD_NAME => $name,
            User::FIELD_EMAIL => $email,
        ];

        if ($password) {
            $attributes[User::FIELD_PASSWORD] = bcrypt($password);
        }

        return $this->repository->update($attributes, $id);
    }

    /**
     * Delete a specific user.
     *
     * @param int $id
     * @return bool
     */
    public function delete(int $id): bool
    {
        return $this->repository->delete($id);
    }
}
