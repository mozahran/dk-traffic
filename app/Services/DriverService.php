<?php

namespace App\Services;

use App\Driver;
use App\Repositories\DriverRepository;
use App\Http\Requests\CodeRequest;

class DriverService
{
    /**
     * @var DriverRepository
     */
    private $driverRepository;

    /**
     * DriverService constructor.
     *
     * @param DriverRepository $driverRepository
     */
    public function __construct(DriverRepository $driverRepository)
    {
        $this->driverRepository = $driverRepository;
    }

    /**
     * Find driver by id.
     *
     * @param int $id
     * @return mixed
     */
    public function find(int $id)
    {
        return $this->driverRepository->find($id);
    }

    /**
     * Create a new driver.
     *
     * @param CodeRequest $request
     * @return mixed
     */
    public function create(CodeRequest $request)
    {
        return $this->driverRepository->create($request->only([
            Driver::FIELD_NAME,
            Driver::FIELD_PHONE_NUMBER,
        ]));
    }

    /**
     * Update an existing driver.
     *
     * @param CodeRequest $request
     * @param $id
     * @return mixed
     */
    public function update(CodeRequest $request, $id)
    {
        return $this->driverRepository->update($request->only([
            Driver::FIELD_NAME,
            Driver::FIELD_PHONE_NUMBER,
        ]), $id);
    }
}