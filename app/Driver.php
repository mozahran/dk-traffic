<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Driver extends Model
{
    /**
     * The table schema.
     */

    const TABLE_NAME = 'drivers';

    const FIELD_PK = 'id';
    const FIELD_NAME = 'name';
    const FIELD_PHONE_NUMBER = 'phone_number';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        self::FIELD_NAME,
        self::FIELD_PHONE_NUMBER,
    ];

    /*
	|--------------------------------------------------------------------------
	| Getters
	|--------------------------------------------------------------------------
    */

    /**
     * Get the user id.
     *
     * @return int
     */
    public function getId()
    {
        return (int) $this->getAttribute(static::FIELD_PK);
    }

    /**
     * Get the name of the user.
     *
     * @return mixed
     */
    public function getName()
    {
        return $this->getAttribute(static::FIELD_NAME);
    }

    /**
     * Get the phone number of the driver.
     *
     * @return mixed
     */
    public function getPhoneNumber()
    {
        return $this->getAttribute(static::FIELD_PHONE_NUMBER);
    }

    /*
	|--------------------------------------------------------------------------
	| Setters
	|--------------------------------------------------------------------------
    */

    /**
     * Set the name of the user.
     *
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->setAttribute(static::FIELD_NAME, $name);
    }

    /**
     * Set the phone number of the user.
     *
     * @param string $phoneNumber
     */
    public function setPhoneNumber(string $phoneNumber)
    {
        $this->setAttribute(static::FIELD_PHONE_NUMBER, $phoneNumber);
    }

    /*
	|--------------------------------------------------------------------------
	| Relationships
	|--------------------------------------------------------------------------
    */

    /**
     * The car that belongs to the driver.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function car()
    {
        return $this->hasOne(Car::class)->withDefault([
            Car::FIELD_MANUFACTURER => trans('misc.unknown'),
            Car::FIELD_MODEL => trans('misc.unknown'),
            Car::FIELD_MODEL_YEAR => trans('misc.unknown'),
            Car::FIELD_DRIVER_ID => null,
        ]);
    }
}
