<?php

use App\Services\CodeService;

Auth::routes();

Route::middleware('auth')->group(function () {

    Route::get('/', 'HomeController@index')->name('home');
    Route::get('/profile', 'HomeController@index')->name('profile');
    Route::resource('codes', 'CodeController')->except(['show']);
    Route::resource('users', 'UserController')->except(['show']);

    Route::get('/codes/reset', function(CodeService $codeService) {
        $codeService->resetSerialNumbers();
        return redirect(route('codes.index'));
    })->name('codes.reset');

});
