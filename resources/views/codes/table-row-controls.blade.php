<div class="btn-group">
    <form action="{{ route('codes.destroy', $code->getId()) }}" method="post" class="form-inline">
        {{ csrf_field() }}
        {{ method_field('DELETE') }}
        <a href="{{ route('codes.edit', $code->getId()) }}" class="btn btn-default btn-sm waves-effect" title="@lang('misc.edit')">
            <i class="fa fa-fw fa-pencil-square-o"></i>
        </a>
        <button type="submit" class="btn btn-default btn-sm waves-effect waves-danger delete-btn"><i class="fa fa-fw fa-trash-o"></i></button>
    </form>
</div>