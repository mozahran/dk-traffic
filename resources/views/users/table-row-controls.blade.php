<div class="btn-group">
    <form action="{{ route('users.destroy', $user->getId()) }}" method="post" class="form-inline">
        {{ csrf_field() }}
        {{ method_field('DELETE') }}
        <a href="{{ route('users.edit', $user->getId()) }}" class="btn btn-default btn-sm waves-effect" title="@lang('misc.edit')">
            <i class="fa fa-fw fa-pencil-square-o"></i>
        </a>
        @if ($user->getId() != auth()->user()->id)
            <button type="submit" class="btn btn-default btn-sm waves-effect waves-danger delete-btn">
                <i class="fa fa-fw fa-trash-o"></i>
            </button>
        @endif
    </form>
</div>