<tr>
    <th class="text-center" style="width: 1%" scope="row">{{ $user->getId() }}</th>
    <td>
        <span class="clearfix">{{ $user->getName() }}</span>
        <span>{{ $user->getEmail() }}</span>
    </td>
    <td class="text-left">
        @include('users.table-row-controls', ['user' => $user])
    </td>
</tr>