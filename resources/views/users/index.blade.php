@extends('layouts.app')

@section('content')

    <div class="m-b-25">
        <a href="{{ route('users.create') }}" class="btn btn-primary waves-effect">
            <i class="fa fa-fw fa-plus-circle"></i> @lang('users.create_user')
        </a>
    </div>

    @isset ($searchQuery)
    <div class="alert alert-info">@lang('users.search_alert_text', ['query' => $searchQuery, 'link' => route('users.index')])</div>
    @endisset

    <div class="card-box">
        <table class="table table-striped table-responsive m-b-0">
            <thead>
                <tr>
                    <th class="text-center" style="width: 1%">#</th>
                    <th>@lang('users.name')</th>
                    <th class="text-left" style="width: 25%">@lang('misc.controls')</th>
                </tr>
            </thead>
            <tbody>
                @each('users.table-row', $users, 'user', 'users.table-empty')
            </tbody>
        </table>
    </div>

    <div class="text-center">
        {{ $users->appends(['q' => $searchQuery])->links() }}
    </div>

@endsection