<div class="text-center m-t-15 m-b-15">
    <a href="{{ $link or '#' }}" class="btn btn-primary"><i class="zmdi zmdi-long-arrow-right"></i> @lang('misc.go_back')</a>
</div>