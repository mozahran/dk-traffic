<?php

return [

    'create_user' => 'إضافة عضوية',
    'edit_user' => 'تعديل عضوية',

    'name' => 'الاسم',
    'email' => 'البريد الإلكتروني',
    'password' => 'كلمة المرور',
    'no_users' => 'لا توجد نتائج في الوقت الحالي!',
    'user_not_found' => 'العضوية المطلوبة غير موجودة!',

    'create_success' => 'تم إضافة عضوية جديدة بنجاح',
    'create_fail' => 'فشل إضافة عضوية جديدة',

    'update_success' => 'تم تعديل البيانات بنجاح',
    'update_fail' => 'فشل تعديل البيانات!',

    'delete_success' => 'تم حذف العضوية بنجاح',
    'delete_fail' => 'فشل حذف العضوية',

    'search_alert_text' => 'عرض النتائج حسب كلمة البحث: <strong>:query</strong> <a href=":link" class="pull-right alert-link">مشاهدة الكل</a>',
];