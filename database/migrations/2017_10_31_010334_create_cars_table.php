<?php

use App\Car;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(Car::TABLE_NAME, function (Blueprint $table) {
            $table->increments(Car::FIELD_PK);
            $table->unsignedInteger(Car::FIELD_DRIVER_ID);
            $table->string(Car::FIELD_PLATE_NUMBER)->unique();
            $table->string(Car::FIELD_MANUFACTURER)->nullable();
            $table->string(Car::FIELD_MODEL)->nullable();
            $table->string(Car::FIELD_MODEL_YEAR)->nullable();
            $table->timestamps();

            $table->index(Car::FIELD_DRIVER_ID);
            $table->index(Car::FIELD_PLATE_NUMBER);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(Car::TABLE_NAME);
    }
}
